$(function () {
    //Навигация по страничкам сайта
    $('body').append(
        '<div style="position: fixed; z-index: 9999; bottom: 0; right: 0; background: #fff; border: solid 1px #000; width: 250px;"> \
            <a href="javascript:void(0);" style="float: right;background:#ccc; color:#000; padding: 5px 10px;position:relative;z-index:20;" onclick="$(this).parent().hide()">Закрыть X</a> \
        <ol> \
            <li><a href="./index(+).html" style="color:#000;">Главная</a></li> \
            <li><a href="./404(+).html" style="color:#000;">404</a></li> \
            <li><a href="./parents-about(+).html" style="color:#000;">Родитель - о проекте</a></li> \
            <li><a href="./parents-choose(+).html" style="color:#000;">Родитель - выбрать бебиситтера</a></li> \
            <li><a href="./parents-create-application(+).html" style="color:#000;">Родитель - разместить объявление</a></li> \
            <li><a href="./parents-current-application(+).html" style="color:#000;">Родитель - текущие объявления</a></li> \
            <li><a href="./parents-invite(+).html" style="color:#000;">Родитель - пригласить</a></li> \
            <li><a href="./parents-messages(+).html" style="color:#000;">Родитель - сообщения</a></li> \
            <li><a href="./parents-orders(+).html" style="color:#000;">Родитель - история заказов</a></li> \
            <li><a href="./parents-payment(+).html" style="color:#000;">Родитель - оплата</a></li> \
            <li><a href="./parents-profile-edit(+).html" style="color:#000;">Родитель - настройки профиля(редактировать)</a></li> \
            <li><a href="./parents-profile(+).html" style="color:#000;">Родитель - настройки профиля</a></li> \
            <li><a href="./parents-reviews(+).html" style="color:#000;">Родитель - отзывы</a></li> \
            <li><a href="./parents-sitter-view(+).html" style="color:#000;">Родитель - просмотр профиля ситтера</a></li> \
            <li><a href="./sitter-invite(+).html" style="color:#000;">Ситтер - приглашения</a></li> \
            <li><a href="./sitter-newsfeed(+).html" style="color:#000;">Ситтер - объявления</a></li> \
            <li><a href="./sitter-order-history(+).html" style="color:#000;">Ситтер - история заказов</a></li> \
            <li><a href="./sitter-profile-edit(+).html" style="color:#000;">Ситтер - настройки профиля(редактировать)</a></li> \
            <li><a href="./sitter-profile(+).html" style="color:#000;">Ситтер - настройки профиля(сохранить)</a></li> \
            <li><a href="./sitter-reviews(+).html" style="color:#000;">Ситтер - отзывы и рейтинг</a></li> \
        </ol> \
    </div>');
});
