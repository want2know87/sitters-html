(function($) {
     $(window).on("load",function(){
        //кастомный скроллы
        $('.rules').mCustomScrollbar({
            axis: "y",
            theme: "dark"
        });

        // $('.messages__contacts').mCustomScrollbar({
        //     axis: "y",
        //     theme: "dark",
        //     live: true
        // });
        // $('.messages__contacts').fadeIn();

        // $('.dialog').mCustomScrollbar({
        //     axis: "y",
        //     theme: "dark",
        //     live: true,
        //     callbacks: {
        //         onInit: function() {
        //             $(this).mCustomScrollbar('scrollTo', 'bottom', {
        //                 scrollInertia: 0
        //             });
        //         },
        //         onUpdate: function() {
        //             $(this).mCustomScrollbar('scrollTo', 'bottom', {
        //                 scrollInertia: 0
        //             });
        //         }
        //     }
        // });
        // $('.dialog').fadeIn();

        // $('.about').mCustomScrollbar({
        //     axis: "y",
        //     theme: "dark"
        // });

        setTimeout(function() {
            $('.layout--parents-invite .sod_list_wrapper').mCustomScrollbar({
                axis: "y",
                theme: "dark"
            });
        }, 1000);

        setTimeout(function() {
            $('.layout--parents-create .sod_list_wrapper').mCustomScrollbar({
                axis: "y",
                theme: "dark"
            });
        }, 1000);


        setTimeout(function() {
            $(".schedule__row .sod_list_wrapper").mCustomScrollbar({
                axis: "yx",
                theme: "dark"
            });
        }, 1000)
    });


})(jQuery);

$(document).ready(function() {
    function supportsFlexBox() {
        var test = document.createElement('test');

        test.style.display = 'flex';

        return test.style.display === 'flex';
    }

    if (supportsFlexBox()) {
        // Modern Flexbox is supported
    } else {
        flexibility(document.documentElement);
    }

    (function() {
        $(".user").on("click", function() {
            $(".user-menu").toggleClass("active");
            $(".user").toggleClass("open");
        });

        $(document).click(function (e){
            var div = $(".user");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                $('.user-menu').removeClass("active");
                $(".user").removeClass("open");
            }
        });

    }());


    (function() {
        //Открытие бургер меню
        $(".burger").on("click", function() {
            $(this).toggleClass("open");
            $(".page-nav").toggleClass("active");
        });


        $(".page-nav__close").on("click", function() {
            $(".page-nav").removeClass("active");
            $(".burger").removeClass("open");
        });

        $(".close").on("click", function(e) {
            e.preventDefault();
            $(".registration-popup").removeClass("open");
            $(".overlay").removeClass("show");
        });

        $(".popup__close").on("click", function(e) {
            e.preventDefault();
            $(".popup").removeClass("open");
            $(".overlay").removeClass("show");
        });

        $(".popup-rating__close").on("click", function(e) {
            e.preventDefault();
            $(".popup-rating").removeClass("open");
            $(".overlay").removeClass("show");
        });

        $(".overlay").click(function() {
            $(".popup").removeClass("open");
            $(".popup-rating").removeClass("open");
            $(".registration-popup").removeClass("open");
            $(".overlay").removeClass("show");
        });


    }());

    (function() {
        //Окно переписки
        $(".contact").on("click", function() {
            $(".messages__window").addClass("active");
        });

        $(".messages__back").on("click", function() {
            $(".messages__window").removeClass("active");
        });
    }());

    (function() {
        var trigger = $(".profile-form__trigger"),
            data = $(".ui-datepicker"),
            input = $(".profile-form__input--data");

        $(document).click(function(e) {
            if (!trigger.is(e.target) && !input.is(e.target)) {

                trigger.removeClass("open");
            }
        });

        input.on("change input", function() {
            if ($(this).val() > "0") {
                trigger.removeClass("open");

            };
        });

        input.on("click", function() {
            trigger.addClass("open");
        });

        trigger.on("click", function(e) {
            e.preventDefault();
            trigger.addClass("open")
            $('.profile-form__input--data').datepicker("show");

        });


        if ($('.profile-form__input--data').length) {
            //Datapicker
            $('.profile-form__input--data').datepicker({
                autoClose:true,
                dateFormat: 'dd-mm-yyyy'
            });
        }

    }());

    (function() {
        //Обрезка текста в сообщения
        if ($('.contact__preview').length) {
            $('.contact__preview').dotdotdot({
                ellipsis: "... ",
                wrap: "word",
                fallbackToLetter: true,
                after: null,
                watch: false,
                height: 50,
                callback: function(isTruncated, orgContent) {},
                lastCharacter: {
                    remove: [" ", ",", ";", ".", "!", "?"],
                    noEllipsis: []
                }
            });
        };

        $(".page-content__greeting-trigger").on("click", function() {
            var triggerText = $(".page-content__greeting-trigger").text();

            if ((triggerText) == 'Развернуть') {
                $(".page-content__greeting-trigger").text("Свернуть");
                $(".page-content__greeting-text--dot").addClass("show");
            } else {
                $(".page-content__greeting-trigger").text("Развернуть");
                $(".page-content__greeting-text--dot").removeClass("show");
            }
        });

    }());

    (function() {

        //Кастомные селекты
        if ($(".profile-form__input--select").length) {
            $(".profile-form__input--select").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };

        if ($("#response").length) {
            $("#response").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };

        if ($(".schedule__select").length) {
            $(".schedule__select").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };

        if ($("#time").length) {
            $("#time").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };
        if ($("#child").length) {
            $("#child").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };

        if ($("#time-after").length) {
            $("#time-after").selectOrDie({
                onChange: function() {
                    $(this).parents('.sod_select').addClass('active');
                }
            });
        };

    }());

    $(function(){
        $(".greeting__link-toggle").click(function () {
            $(".greeting__toggle").slideToggle();
            if($(this).text() == "Свернуть"){
                $(this).text("Развернуть");
            } else {
                $(this).text("Свернуть");
            }
        });
    });
}); //Конец ready
